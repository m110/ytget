#
# YouTube video parser
# by m110
# October / November 2013
#

package Parser;

use POSIX;
use URI::Encode qw(uri_decode);

# How many times should the script try to get proper URL
my $retries = 3;

sub new {
    my ($class, $id, $options) = @_;

    my $self = {
        id => $id,
        title => 'YouTube Video',
    };
    
    bless $self, $class;

    print "Video ID: $self->{id}\n";
    print "Looking for video URL...\n";

    # Parse -r option
    if (defined $options->{r} && $options->{r} > 0) {
        $retries = $options->{r};
    }

    my $tries = 0;
    while (1) {
        # Download source
        $self->save_source();
        $self->save_download_url();

        my $spider = `wget --spider -S "$self->{download_url}" 2>\&1`;
        my ($size) = $spider =~ /Content-Length: (\d+)/;
        
        if (defined $size) {
            $self->{size} = $size;
            last;
        } else {
            $i++;
            print "Couldn't find valid video URL, retrying... ($i of $retries)\n";

            if ($i >= $retries) {
                die("Parsing video URL failed.\n");
            }
        }
    }

    # Save video title
    $self->save_title();
    # Set the default file name
    $self->save_default_filename();
    
    # Is output file name provided by user?
    my $output_file = $options->{o};
    if (defined $output_file) {
        # Was directory provided?
        if (-d $output_file) {
            $self->{filename} = "$output_file/$self->{filename}";
        } else {
            # Check is the file can be created
            system("touch $output_file &>/dev/null");
            if ($? != 0) {
                die("Output file could not be created.\n");
            }

            $self->{filename} = $output_file;
        }
    }

    print "Title: $self->{title}\n";

    return $self;
}

sub save_source {
    my $self = shift;
    $self->{source} = `wget -Ncq -e "convert-links=off" --keep-session-cookies \\
                       --save-cookies /dev/null --no-check-certificate "$self->{id}" -O-`
                       or die "Error while downloading source.\n";
}

sub save_title {
    my $self = shift;

    ($self->{title}) = $self->{source} =~ /<title>(.+)<\/title>/si;
}

sub save_default_filename {
    my $self = shift;
    
    my $title = $self->{title};
    $title =~ s/[^\w\d]+/_/g;
    $title =~ s/_youtube//ig;
    $title =~ s/^_//ig;
    $title =~ s/_$//ig;
    $self->{filename} = $title . ".webm";
}

# Strip unused parts and convert some symbols
sub save_download_url {
    my $self = shift;

    # Grab the stream
    my ($download_url) = $self->{source} =~ /"url_encoded_fmt_stream_map"([\s\S]+?)\,/ig;

    # Process the URL
    $download_url = uri_decode($download_url);
    $download_url =~ s/\:\ \"//;
    $download_url =~ s/sig=/signature=/g;
    $download_url =~ s/\\u0026/&/g;
    $download_url =~ s/(type=[^&]+)//g;
    $download_url =~ s/(fallback_host=[^&]+)//g;
    $download_url =~ s/(quality=[^&]+)//g;

    # Collect the url and signature
    my ($signature) = $download_url =~ /(signature=[^&]+)/;
    my ($youtube_url) = $download_url =~ /(http.+)/;
    $youtube_url =~ s/&signature.+$//;

    # Combine the url and signature
    my $download_url = "$youtube_url\&$signature";
    $download_url =~ s/&+/&/g;
    $download_url =~ s/&itag=\d+&signature=/&signature=/g;
    
    $self->{download_url} = $download_url;
}

sub download {
    my $self = shift;

    print "Starting video download...\n";
    print "File name: $self->{filename}\n";
    print "File size: $self->{size}\n";

    system(qq(wget -Ncq -e "convert-links=off" --load-cookies /dev/null \\
              --tries=50 --timeout=45 --no-check-certificate "$self->{download_url}" \\
              -O $self->{filename} &));

    unless (defined $self->{size}) {
        die("Something went wrong. Is the URL correct?\n");
    }

    unlink $self->{filename} if (-e $self->{filename});

    # Print progress
    my $length = 30;
    while (1) {
        my $download_size = -s $self->{filename};
        my $percent = ceil(100 * $download_size / $self->{size});
        my $bar_percent = ceil($percent / (100 / $length));

        print "\r[" . "=" x $bar_percent . " " x ($length - $bar_percent) . "] ($percent%) $download_size / $self->{size}";

        last unless $download_size < $self->{size};
    }

    print "\n";
}

1;
